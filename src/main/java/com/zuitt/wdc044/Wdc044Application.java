package com.zuitt.wdc044;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

// To specify the main class of the Springboot application
@SpringBootApplication
//Tells the spring boot that this will handle endpoints.
@RestController
public class Wdc044Application {

	public static void main(String[] args) {
		SpringApplication.run(Wdc044Application.class, args);
	}

	//This is used for Mapping HTTP GET requests
	@GetMapping("/hi")
	// "@RequestParam" is used to extract parameters.
	//name = john; Hello John.
	//name = World; Hello World.
	public String hello(@RequestParam(value = "name", defaultValue = "user!") String name){
		return String.format("Hi %s", name);
	}
}